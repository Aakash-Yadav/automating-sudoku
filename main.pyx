cimport numpy
import lxml.html 


cdef save_file(numpy.ndarray k):
    with open('solution/sudoku_solve.py','w') as f:
        f.write(f'solve = {k.tolist()}')
    return 1 

cdef open_html_file():
    with open('html_file/array.html','r') as f:
        data = f.read();
    return data

cdef convert_html_to_array_(numpy.ndarray Zero_array):
    data = lxml.html.fromstring(open_html_file())
    Td_of_html = data.xpath(".//td");
    cdef short int i;
    for i in range(81):
        value_to_add = Td_of_html[i].text_content();
        if value_to_add == "\xa0":
            Zero_array[i]=0;
        else:
            Zero_array[i]= int(value_to_add);
    return Zero_array;


cdef is_valid_move(numpy.ndarray grid ,short int row,short int col,short int number):
    
    cdef short int x,y

    for x in range(9):
        if grid[row][x] == number:
            return 0;

    for x in range(9):
        if grid[x][col] == number:
            return 0;
    
    cdef short int box_x = (col//3)*3;
    cdef short int box_y =(row//3)*3;

    for x in range(3):
        for y in  range(3):
            if grid[box_y+x][box_x+y] == number:
                return 0;
    return 1

cdef solve(numpy.ndarray grid):
    cdef short int i , j ,n;
    for i in range(9):
        for j in range(9):
            if grid[i][j] == 0:
                for n in range(1,10):
                    if is_valid_move(grid,i,j,n):
                        grid[i][j] = n;
                        solve(grid);
                        grid[i][j] = 0;
                return  
    
    cdef short int on_of = 1
    cdef numpy.ndarray out_put = grid.reshape(-1)
    for i in range(81):
        if out_put[i] == 0:
            on_of = 0    
    if on_of:
        save_file(out_put)
    
    return 1;

cpdef Solve(numpy.ndarray G):
    return solve(G);

cpdef convert_html_to_array(numpy.ndarray Zero_array):
    return convert_html_to_array_(Zero_array)
