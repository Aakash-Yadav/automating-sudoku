from numpy import zeros,reshape
from concurrent.futures import ProcessPoolExecutor
from pyautogui import (hotkey , press)
'Cython import'
import main


def Process(fun,value,Key=0):
    if Key:
        with ProcessPoolExecutor() as exe:
            k=exe.submit(fun,value)
        return k.result()

def automation_keyboard(matrix,GRID_2):
    for i in range(81):
        if GRID_2[i] == 0:
            press(str(matrix[i]))
            hotkey('right')
        else:
            hotkey('right')
    return 0

if __name__ == '__main__':
    html_data_array =  Process(main.convert_html_to_array,zeros((9*9),dtype=int),Key=1)
    k=reshape(html_data_array,(9,9))
    Process(main.Solve,k,Key=1);
    print(k,"\n")
    from time import sleep
    sleep(3)
    from solution import sudoku_solve
    print(reshape(sudoku_solve.solve,(9,9))) 
    automation_keyboard(sudoku_solve.solve,html_data_array.reshape(-1))
