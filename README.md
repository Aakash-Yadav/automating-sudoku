# Sudoku Solver in Cython
Build a sudoku solver in Cython today! Sudoku Puzzle is a very popular puzzle that appears in the daily newspaper that attracts the attention of a lot of people. There are a lot of difficult, unsolved problems about sudoku puzzles and their generalizations which makes this puzzle interesting, specifically to a lot of mathematics lovers.

# What is a Sudoku Puzzle ?
Sudoku is a logic-based, combinatorial number-placement puzzle. In classic sudoku, the objective is to fill a 9 × 9 grid with digits so that each column, each row, and each of the nine 3 × 3 subgrids that compose the grid contain all of the digits from 1 to 9

# What is Cython ?
Cython is a programming language that aims to be a superset of the Python programming language, designed to give C-like performance with code that is written mostly in Python with optional additional C-inspired syntax. Cython is a compiled language that is typically used to generate CPython extension modules.
[Cython Documentation](URL 'https://docs.cython.org/en/latest/')
## What is  PyAutoGUI?
PyAutoGUI lets your Python scripts control the mouse and keyboard to automate interactions with other applications. The API is designed to be simple. PyAutoGUI works on Windows, macOS, and Linux, and runs on Python 2 and 3.
[PyAutoGUI Documentation](URL 'https://pyautogui.readthedocs.io/en/latest/')


# Command to compile cython file
` python3 setup.py build_ext --inplace `

# Testing (PvP)
use this code to play [livesudoku.com](URL 'https://www.livesudoku.com/')
bot solve Sudoku by getting Html (Tbody) 
###### Average time to win = 00:15 second 

### Conclusion;
And that’s it for building a sudoku solver in Cython! I hope you had fun reading through the article and learned how we implemented the code.
Psst… there’s also an easier way to build a sudoku solver in Cython!

### thank you.
